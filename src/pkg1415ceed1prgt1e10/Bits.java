/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt1e10;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Bits {

  public static void main(String args[]) {
    int a;

    a = 2;
    String b = Integer.toBinaryString(a);
    System.out.println("1. Decimal:" + a + "  Binario:" + b);

    a = a << 1;  // Desplaza 1 bit izq, num=num*2
    b = Integer.toBinaryString(a);
    System.out.println("2. Decimal:" + a + "  Binario:" + b);

    a = 2;
    a = a >> 1;  // Desplaza 1 bit der, num=num/2
    b = Integer.toBinaryString(a);
    System.out.println("3. Decimal:" + a + "  Binario:" + b);


  }
}
/* EJECUCION:
 1. Decimal:2  Binario:10
 2. Decimal:4  Binario:100
 3. Decimal:1  Binario:1
 */
