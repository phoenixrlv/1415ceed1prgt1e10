/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt1e10;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Operadores {

  public static void main(String args[]) {
    int a = 4, b = 2, c = 2, op = 0;
    System.out.println("a=" + a + " b=" + b + " c=" + c);
    op = a + b;
    System.out.println("a+b=" + op);
    op = a - b;
    System.out.println("a-b=" + op);
    op = a * b;
    System.out.println("a*b=" + op);
    op = a / b;
    System.out.println("a/b=" + op);
    op = a % b;
    System.out.println("a%b=" + op);
    op = a + b * c;
    System.out.println("a+b*c=" + op);
  }
}
/* EJECUCION:
 a=4 b=2 c=2
 a+b=6
 a-b=2
 a*b=8
 a/b=2
 a%b=0
 a+b*c=8
 */
