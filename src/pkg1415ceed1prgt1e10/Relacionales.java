/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt1e10;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Relacionales {

  public static void main(String args[]) {
    int a = 4, b = 2;
    boolean op;
    System.out.println("a=" + a + " b=" + b);
    op = a < b;
    System.out.println("a<b es " + op);
    op = a > b;
    System.out.println("a>b es " + op);
    op = a <= b;
    System.out.println("a<=b es " + op);
    op = a >= b;
    System.out.println("a>=b es " + op);
    op = a != b;
    System.out.println("a!=b es " + op);
    op = a == b;
    System.out.println("a==b es " + op);
  }
}
/* EJECUCION:
 a=4 b=2
 a<b es false
 a>b es true
 a<=b es false
 a>=b es true
 a!=b es true
 a==b es false
 * */
